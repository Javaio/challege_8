const auth = require('./auth');
const Oauth = require('./Oauth');
const media = require('./media');
const userhis = require('./user_history');
const userbio = require('./user_biodata');

module.exports = {
    auth, Oauth, media, userhis, userbio
};