const express = require('express');
const router = express();
const handler = require('../handlers');

router.get('/auth/register', handler.auth.signUp);
router.post('/auth/register', handler.auth.register);

router.get('/auth/login', handler.auth.signIn);
router.post('/auth/login', handler.auth.login);

router.get('/auth/login/google', c.google);
router.get('/auth/login/facebook', c.facebook);

router.get('/auth/forgot-password', handler.auth.forgotPasswordView);
router.post('/auth/forgot-password', handler.auth.forgotPassword);

router.get('/auth/reset-password', handler.auth.resetPasswordView);
router.post('/auth/reset-password', handler.auth.resetPassword);

router.post('/upload/single', media.single('media'), (req, res) => {
    const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename;
    
    return res.json({
        imageUrl
    });
});

router.post('/upload/multiple', media.array('media'), (req, res) => {

    const files = []
    req.files.forEach(file => {
        const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + file.filename;

        files.push(imageUrl)
    })

    return res.json(files);
});

module.exports = router;